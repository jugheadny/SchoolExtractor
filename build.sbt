name := "SchoolExtractor"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies ++= {
  val ioAkkaVersion = "2.4.0"
  Seq(
    // Test
    "com.typesafe.akka" %% "akka-testkit" % ioAkkaVersion % "test",
    "org.scalatest" %% "scalatest" % ioAkkaVersion % "test",
    "org.jsoup" % "jsoup" % "latest.integration",

    // Akka
    "com.typesafe.akka" %% "akka-actor" % ioAkkaVersion

    //  // Change this to another test framework if you prefer
    //  //"com.typesafe.akka" %% "akka-remote" % "2.3.5",
//  "org.scalatest" %% "scalatest" % ioAkkaVersion % "test",
  )
}