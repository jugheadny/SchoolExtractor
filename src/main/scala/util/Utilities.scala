/**
 * Created by rcarroll on 9/27/15.
 */
package util

object Utilities {
  def toInt(s: String): Option[Int] = {
    try {
      Some(s.toInt)
    } catch {
      case e: Exception => None
    }
  }
}
