/**
 * Created by rcarroll on 9/18/15.
 */
package util

case class Uri(
                protocol: String, userInfo: String,
                host: String, port: Int,
                path: String, query: String, fragment: String
                ) {
  lazy val javaURI = {
    new java.net.URI(
      protocol, userInfo,
      host, port,
      path, query, fragment
    )
  }

  override def toString = {
    javaURI.toString
  }
}

object Uri {
  def apply(uri: String): Uri = {
    val parsedUri = new java.net.URI(uri)
    Uri(
      parsedUri.getScheme, parsedUri.getUserInfo,
      parsedUri.getHost, parsedUri.getPort,
      parsedUri.getPath, parsedUri.getQuery, parsedUri.getFragment
    )
  }
}