/**
 * Created by rcarroll on 9/18/15.
 */
package pojo

case class School() {
  var name: String = null
  var address: String = null
  var ranking: String = null
  var district: District = null
  var medal: String = null
  var studentTeacherRatio: String = null
  var collegeReadiness: String = null
  var mathProficiency: String = null
  var englishProficiency: String = null
}

