/**
 * Created by rcarroll on 9/18/15.
 */
package pojo

import util.Uri

case class Page(pageNumber : Int, url : Uri)
