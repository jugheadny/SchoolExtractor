/**
 * Created by rcarroll on 9/27/15.
 */
package pojo

import util.Uri

case class District(name : String, url : Uri)