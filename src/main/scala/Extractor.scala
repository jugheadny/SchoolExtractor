/**
 * Created by rcarroll on 9/28/15.
 */

import akka.actor.{Actor, ActorLogging, Props}
import pojo.School

object Extractor {
  def props() : Props = Props(new Extractor)
}

class Extractor extends Actor with ActorLogging {
  def receive = {
    case school : School => {
      log.info("Received School")
      log.info(s"${school.ranking}") //${school.name} ${school.address}  ${school.district.name}")
    }
  }
}
