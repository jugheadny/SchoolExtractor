/**
 * Created by rcarroll on 9/28/15.
 */

import akka.actor.{Actor, ActorLogging, Props}
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import pojo._
import util.{Uri, Utilities}

import scala.collection.JavaConversions._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

object Processor {
  def props() : Props = Props(new Processor)
}

class Processor extends Actor with ActorLogging {
  def receive = {
    case Page(pageNumber, url) => {
      log.info(s"Received Page: ${pageNumber} - Url: ${url.toString}")

      if (pageNumber == 1) {
        context.system.shutdown()
      }

      val result = Future {
        val client = sender()
        val doc = Jsoup.connect(url.toString).get()

        (doc, client)
      }

      result onComplete {
        case Success(r) => {
          val doc = r._1
          val client = context actorOf Extractor.props
          val schools = extractSchools(doc)
          val total = totalPages(doc)

          log.info(s"Total Pages: ${total}")

          // Send school message
          for (school <- schools) {
            client ! school
          }

          val pages = pagination(doc, url)
          for (page <- pages) {
            self.tell(page, client)
          }
        }

        case Failure(t) => {
          val client = context.actorOf(Extractor.props())

          println("An error has occured: " + t.getMessage)
          self.tell(Page(pageNumber, url), client)

          //context.system.shutdown()
        }
      }
    }

    case _ => log.info("Opps!")
  }

  def totalPages(doc: Document) = {
    val pages = doc.select("#pagination > a.pager_link")
    val last = pages.last()

    val text = last.text()
    if (text == ">") {
      pages.get(pages.size() - 2).text()
    } else {
      text
    }
  }

  def retrieveCurrentPage(doc: Document) = {
    doc.select("#pagination > span.pager_curpage").first().text()
  }

  def extractSchools(doc: Document) = {
    var schools = List[School]()

    val divs = doc.select("tbody > tr")
    divs.foreach(div => {

      val rankingElem = div.select(".rankings-score")
      val schoolNameElem = div.select(".school-name")
      val schoolAddressElem = div.select(".school-address")
      val schoolDistrictElem = div.select(".school-district")
      val schoolDistrictLink = schoolDistrictElem.select("a[href]").attr("href")

      val medalElem = div.select(".medal_block")
      val studentTeacherRatio = div.select(".studentteachers .lead-value")
      val collegeReadiness = div.select(".collegereadiness .lead-value")
      val mathProficiency = div.select(".math .lead-value")
      val englishProficiency = div.select(".english .lead-value")

      val school = new School()
      school.name = schoolNameElem.text()
      school.address = schoolAddressElem.text()
      school.district = new District(schoolDistrictElem.text(), Uri(schoolDistrictLink))
      school.ranking = rankingElem.text()
      school.medal = medalElem.text()
      school.studentTeacherRatio = studentTeacherRatio.text()
      school.collegeReadiness = collegeReadiness.text()
      school.mathProficiency = mathProficiency.text()
      school.englishProficiency = englishProficiency.text()

      schools ::= school
    })

    schools
  }

  def pagination(doc: Document, uri: Uri) = {
    var pages = List[Page]()

    val linkElements = doc.select("#pagination>.pager_curpage + a")
    if (linkElements.length != 0) {
      val link = linkElements.first()
      val number = Utilities.toInt(link.text()).getOrElse(0)

      if (number != 0) {
        val url = Uri(link.attr("href"))
        val k = uri.copy(query = url.query)

        val page = new Page(number, k)
        pages ::= page
      }
    }
    pages
  }
}
