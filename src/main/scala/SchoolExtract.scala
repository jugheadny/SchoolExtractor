
import akka.actor.ActorSystem
import pojo.Page
import util.Uri

object SchoolExtract extends App {
  var system = ActorSystem("SchoolExtractor")
  val p = system.actorOf(Processor.props())

  p ! Page(0, Uri("http://www.usnews.com/education/best-high-schools/florida/rankings"))

  system.awaitTermination()
}
